var mongoose = require('mongoose');

var userSchema = mongoose.Schema({
    firstname: String,
    lastname: String
});

module.exports = {'userModel': mongoose.model('User', userSchema)};
