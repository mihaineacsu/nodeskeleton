var config = require('./config'),
    express = require('express'),
    mongoose = require('mongoose'),
    bodyParser = require('body-parser');
    routes = require('./endpoints');
    // models = require('./models');

var app = express();

app.set('view engine', 'html');
app.engine('html', require('hbs').__express);
app.use(express.static(__dirname + '/public'));
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
app.use('/', routes);

mongoose.connect(config.db);

var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
	  console.log('Connected to mongodb, all good');
});

console.log('Server listening on ' + config.port);
app.listen(config.port);
