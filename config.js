var extend = require('util')._extend;

var local = {db: 'mongodb://localhost/testnode',
			port: 8000};

/**
 * If you decide to use heroku to host/test your app you 
 * fill in config variables here 
 */
var heroku = {db: 'remoteMongoURL',
			  port: process.env.PORT};


/**
 * Expose
 */

module.exports = {
  development: extend(local),
  production: extend(heroku)
}[process.env.NODE_ENV || 'development'];
