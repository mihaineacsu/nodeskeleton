var express = require('express'),
    router = express.Router(),
    models = require('./models.js')

// You can test this endpoint by accessing:
// http://localhost:8000/test?category=dresses&color=green
router.get('/test', function(req, res){
	var message = 'GET request on /test' + JSON.stringify(req.query);

	console.log(message);
	res.send(message);
});

// You can test this endpoint by using Chrome's POSTMAN extension
// which will make it really easy to perform POST requests on a
// specific URL
router.post('/test', function(req, res){
	var message = 'POST request on /test';
	if (Object.keys(req.body).length)
		message = message + ' with params:' + JSON.stringify(req.body);

	console.log(message);
	res.send(message);
});


router.get('/form', function(req, res){
	console.log('GET request on /form; redirecting to public /simpleform.html')
	res.redirect(302, '/simpleform.html');
});

router.post('/form', function(req, res){
	console.log('POST request on /form; rendering view formResponse');
	var userModel = models.userModel;

	if (req.body.firstname && req.body.lastname){
		var newUser = new userModel({firstname: req.body.firstname, 
									 lastname: req.body.lastname});

		newUser.save(function (err) {
			if (err){
				return handleError(err);
			}

			console.log('Saved new entry in mongodb')
		});

		res.render('formResponse', req.body);
	}
	else
		res.redirect(302, '/form');
});


router.get('/users', function(req, res){
	var userModel = models.userModel;

	userModel.find(function(err, users){
		if (err)
			return console.error(err);

		console.log('GET request on /users; found following users in db: ' + JSON.stringify(users));
		res.send(users);
	});
});


// The 404 Route (ALWAYS Keep this as the last route)
// If it doesn't match any of the above routes it will enter this one
router.get('*', function(req, res){
  res.send('Oooops, not found!',404);
});

// The following line will make the router object available to our app.
// When this file is being 'required' from another file, only the router
// object will be imported in that file
module.exports = router;
